#include <cassert>
#include <iostream>
#include <string>
#include <thread>
#include <boost/asio.hpp>

#include "utils.h"

#include "ktulu_model/game_manager.hpp"
#include "net/core.h"

#include "config.h"

int main(int argc, char **argv) {

    boost::asio::io_service io_service;
    boost::asio::io_service::work work(io_service);
    short ktulu_port = DEFAULT_GAME_PORT;

    //auto game_manager = create<Ktulu::GameManager>(io_service, ktulu_port);
    Ktulu::GameManager game_manager(io_service, ktulu_port);
    game_manager.start();
    auto game_manager_ptr = std::shared_ptr<Ktulu::GameManager>(&game_manager);
    auto replicator = create<Server::Replicator>(game_manager_ptr, 2, DEFAULT_REPLICATOR_PORT);
    replicator->start();

    auto broadcaster = create<Server::Broadcaster>(DEFAULT_SEARCHER_PORT, SERVER_HELLO_MESSAGE "\n", game_manager_ptr);
    broadcaster->start();
    std::cerr << "Teraz run!!!\n";
    io_service.run();
    std::cerr << "I po runie\n";

   
      std::string ignored;
      std::cin >> ignored;
    
    return 0;
}
