#include <boost/regex.hpp>
#include <condition_variable>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <string>
#include <thread>
#include <unordered_map>

#include "config.h"
#include "debug_print.h"
#include "ktulu_model/game.h"
#include "replicator.h"
#include "server_searcher.h"

using Command = std::pair<boost::regex, std::function<std::string(const boost::cmatch &cmd)>>;

namespace Server {
  /*
   * BackuperServer
   */
  class BackuperSession
    : public std::enable_shared_from_this<BackuperSession>
  {
    private:
      using tcp = boost::asio::ip::tcp;

      tcp::socket socket;
      enum {
        buffer_length = 1024,
        max_buffer_length = 102400,
      };
      char data[buffer_length+1];
      std::size_t data_len;

      char *rdata;
      char *rdata_send;
      std::size_t rdata_len;

      std::stringstream cmd_buffer;

      ReplicatorPtr replicator;

    public:
      BackuperSession(tcp::socket &&socket, ReplicatorPtr replicator)
        : socket(std::move(socket))
          , replicator(replicator)
      {
      }

      void start()
      {
        do_read();
      }

      void do_read()
      {
        auto self(shared_from_this());
        socket.async_read_some(boost::asio::buffer(data, buffer_length)
            , [this,self](boost::system::error_code error, std::size_t length)
              {
                if (not error) {
                  data[length] = '\0';

                  auto data_it = data;
                  std::stringstream response;
                  for (int i = 0; i < length; ++i) {
                    if (data[i] == '\n') {
                      data[i] = '\0';
                      cmd_buffer << data_it;
                      data_it = data + i + 1;

                      PackedGame game;
                      response << replicator->exec(cmd_buffer.str());
                      cmd_buffer.str("");
                    }
                  }
                  cmd_buffer << data_it;

                  if (cmd_buffer.str().size() > max_buffer_length) {
                    return;
                  }

                  if (not response.str().empty()) {
                    rdata = new char[response.str().size()+1];
                    std::strcpy(rdata, response.str().c_str());
                    rdata_send = rdata;
                    rdata_len = std::strlen(rdata);
                    do_write();
                  } else {
                    do_read();
                  }
                }
              }
        );
      }

      void do_write()
      {
        auto self(shared_from_this());
        boost::asio::async_write(socket, boost::asio::buffer(rdata, rdata_len)
            , [this,self](boost::system::error_code error, std::size_t length)
              {
                if (not error) {
                  if (length < rdata_len) {
                    rdata_len -= length;
                    rdata_send += length;
                    do_write();
                  } else {
                    delete[] rdata;
                    do_read();
                  }
                }
              }
        );
      }

      bool game_ready()
      {
        return true;
      }
  };

  class BackuperServer
  {
    private:
      using tcp = boost::asio::ip::tcp;

      tcp::socket socket;
      tcp::acceptor acceptor;
      ReplicatorPtr replicator;

    public:
      BackuperServer(boost::asio::io_service &io_service, short port
          , ReplicatorPtr replicator)
        : acceptor(io_service, tcp::endpoint(tcp::v4(), port))
          , socket(io_service)
          , replicator(replicator)
      {
        do_accept();
      }

      void do_accept()
      {
        acceptor.async_accept(socket
            , [this](boost::system::error_code error){
              if (not error) {
                std::make_shared<BackuperSession>(std::move(socket), replicator)->start();
              }

              do_accept();
            });
      }
  };

  /*
   * Replicator
   */
  class Replicator::Impl {
    public:
      Ktulu::GameManagerPtr GameManager;
      timeout_t watcher_timeout;
      bool watcher_running;
      short port;

      std::string hello_message;

      std::unique_ptr<std::thread> watcher_thread;

      std::mutex watcher_mutex;
      std::condition_variable watcher_sleep;

      std::unordered_map<Ktulu::game_id_t, PackedGame> games;

      boost::asio::io_service backuper_io_service;
      std::unique_ptr<std::thread> backuper_thread;
      std::unique_ptr<boost::asio::io_service::work> backuper_io_service_work;
      std::unique_ptr<BackuperServer> backuper_server;

      std::vector<Command>commands;
  };

  Replicator::Replicator(Ktulu::GameManagerPtr GameManager, timeout_t timeout, short port)
    : pimpl(new Impl{GameManager, timeout, false, port})
  {
    Debug::dbg_perr("replicator: create GameManager_id:", pimpl->GameManager->id());
    std::srand(std::time(NULL));

    {
      std::stringstream ss;
      ss << SERVER_HELLO_MESSAGE << " ID " << pimpl->GameManager->id() << " REPLICATOR\n";
      pimpl->hello_message = ss.str();
      Debug::dbg_perr("replicator: hello_message:", pimpl->hello_message);
    }

    init_commands();
  }

  Replicator::~Replicator()
  {
    close();
  }

  void Replicator::run_watcher()
  {
    std::unique_lock<std::mutex> lock(pimpl->watcher_mutex);
    pimpl->watcher_running = true;

    Debug::dbg_perr("replicator: watcher started");

    while (true) {
      pimpl->watcher_sleep.wait_for(lock
          , std::chrono::seconds(pimpl->watcher_timeout)
          , [this](){
            return not pimpl->watcher_running;
          });
      if (not pimpl->watcher_running) {
        break;
      }
      Debug::dbg_perr("replicator: watching");
      auto servers = Ktulu::search_servers(DEFAULT_SEARCHER_PORT, 1, pimpl->hello_message);
      if (not servers.empty()) {
        auto server = servers[std::rand() % servers.size()];
        Debug::dbg_perr("replicator: replicating to:", server);
      }
    }
    Debug::dbg_perr("replicator: watcher stopped");
  }

  void Replicator::run_backuper()
  {
    pimpl->backuper_server.reset(new BackuperServer(pimpl->backuper_io_service, pimpl->port, self()));
    pimpl->backuper_io_service_work.reset(new boost::asio::io_service::work(pimpl->backuper_io_service));
    pimpl->backuper_io_service.run();
  }

  void Replicator::start()
  {
    pimpl->watcher_thread.reset(new std::thread(&Replicator::run_watcher, this));
    pimpl->backuper_thread.reset(new std::thread(&Replicator::run_backuper, this));
  }

  void Replicator::close()
  {
    {
      std::lock_guard<std::mutex> lock(pimpl->watcher_mutex);
      pimpl->watcher_running = false;
    }

    pimpl->watcher_sleep.notify_all();

    pimpl->watcher_thread->join();
    pimpl->watcher_thread.reset();

    pimpl->backuper_io_service_work.reset();
    pimpl->backuper_io_service.stop();
    if (pimpl->backuper_thread.get() != NULL) {
      pimpl->backuper_thread->join();
      pimpl->backuper_thread.reset();
    }
  }

  std::string Replicator::exec(std::string command)
  {
    if (command.empty()) {
      return "";
    }

    boost::cmatch match;
    for (auto c : pimpl->commands) {
      if (boost::regex_match(command.c_str(), match, c.first)) {
        return c.second(match);
      }
    }
    return UNKNOWN_COMMAND_MSG "\n";
  }

  void Replicator::init_commands()
  {
    pimpl->commands.push_back(Command(boost::regex("\\s*help\\s*", boost::regex::icase)
        , [](const boost::cmatch &cmd){
          return OK_MSG " you should know what to do\n";
        }));

    pimpl->commands.push_back(Command(boost::regex(
            "\\s*get\\s+game"
            "\\s+id\\s+(\\d+)"
            "\\s*", boost::regex::icase)
        , [](const boost::cmatch &cmd){
          std::stringstream response;
          Ktulu::game_id_t id = std::stoll(cmd[1]);
          response << OK_MSG;
          response << "\n";
          return response.str();
        }));

    pimpl->commands.push_back(Command(boost::regex(
            "\\s*backup\\s+game"
            "(.*)", boost::regex::icase)
        , [](const boost::cmatch &cmd){
          {
            std::string data = cmd[1];
            boost::regex regex(
                "\\s+(.*)", boost::regex::icase);
            boost::cmatch match;
            if (not boost::regex_match(data.c_str(), match, regex)) {
              return FAIL_MSG "\n";
            }
          }

          PackedGame game;
          if (not game.decode(cmd[1])) {
            return FAIL_MSG "\n";
          }

          Debug::dbg_perr("BACKUP GAME: ", game.encode());

          return OK_MSG "\n";
        }));
  }

  bool PackedGame::decode(std::string data)
  {
    int players_count;

    {
      boost::regex regex(
          "\\s*id\\s+(\\d+)"
          "\\s+day\\s+(\\d+)"
          "\\s+action\\s+(\\d+)"
          "\\s+players\\s+(\\d+)"
          "(.*)", boost::regex::icase);
      boost::cmatch match;
      if (not boost::regex_match(data.c_str(), match, regex)) {
        return false;
      }
      game_id = std::stoll(match[1]);
      day_no = std::stoll(match[2]);
      action_no = std::stoll(match[3]);
      players_count = std::stoll(match[4]);
      data = match[5];
    }

    {
      std::stringstream regex_str;
      for (int i = 0; i < players_count; ++i) {
        regex_str << "\\s+(\\d+)";
      }
      regex_str << "(.*)";
      boost::regex parse_players(regex_str.str());
      boost::cmatch match;
      if (not boost::regex_match(data.c_str(), match, parse_players)) {
        return false;
      }
      players.clear();
      for (int i = 0; i < players_count; ++i) {
        players.push_back(std::stoll(match[i+1]));
      }
      data = match[match.size()-1];
    }

    {
      boost::regex parse_players("\\s*");
      boost::cmatch match;
      if (not boost::regex_match(data.c_str(), match, parse_players)) {
        return false;
      }
    }

    return true;
  }

  std::string PackedGame::encode() const
  {
    std::stringstream result;

    result << "id " << game_id
        << " day " << day_no
        << " action " << action_no
        << " players " << players.size();
    for (auto player_id : players) {
      result << " " << player_id;
    }
    return result.str();
  }
}
