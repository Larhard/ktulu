#ifndef SERVER_BROADCASTER_H
#define SERVER_BROADCASTER_H

#include <memory>
#include <string>

#include "factory.h"
#include "header.h"
#include "ktulu_model/header.h"
#include "ktulu_model/game_manager.hpp"

namespace Server {
    class Broadcaster : public Producible<Broadcaster> {
        /*
         * UDP ping responder sending server hello message
         */

        private:
            class Impl;
            std::unique_ptr<Impl> pimpl;

        public:
			// TODO: Should be more friendly
            Broadcaster(short port, const std::string hello_message, Ktulu::GameManagerPtr model);
            ~Broadcaster();

            void run();
            /*
             * Execute blocking broadcaster process
             */

            void start();
            /*
             * Execute run() in separate thread
             */

            void close();
            /*
             * Close all connections
             *
             * It is called by default at the end of object lifetime by
             * destructor
             */

            Ktulu::GameManager::id_type model_id() const;
    };
}

#endif // SERVER_BROADCASTER_H
