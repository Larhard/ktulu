#include <boost/asio.hpp>
#include <boost/regex.hpp>
#include <memory>
#include <string>
#include <thread>

#include "server_broadcaster.h"
#include "config.h"
#include "server_broadcaster.h"
#include "utils.h"

namespace Server {

  /*
   * BroadcastServers
   */

  using Debug::dbg_perr;

  class SimpleBroadcastServer {
    private:
      using udp = boost::asio::ip::udp;

      enum {
        buffer_length = 1024
      };

      char data[buffer_length+1];
      char rdata[buffer_length+1];
      udp::endpoint sender_endpoint;
      udp::socket socket;

      std::string _hello_message;

      void do_receive();
      void do_send(std::size_t data_len);

      Broadcaster *broadcaster;

    public:
      SimpleBroadcastServer(boost::asio::io_service &io_service, short port
          , std::string hello_message);
      ~SimpleBroadcastServer();

      std::string hello_message() const;
      void set_broadcaster(Broadcaster *broadcaster);
  };

  SimpleBroadcastServer::SimpleBroadcastServer(boost::asio::io_service &io_service
      , short port, std::string hello_message)
    : socket(io_service, udp::endpoint(udp::v4(), port))
      , _hello_message(hello_message)
  {
    do_receive();
  }

  SimpleBroadcastServer::~SimpleBroadcastServer()
  {
  }

  std::string SimpleBroadcastServer::hello_message() const {
    return _hello_message;
  }

  void SimpleBroadcastServer::set_broadcaster(Broadcaster *broadcaster)
  {
    this->broadcaster = broadcaster;
  }

  void SimpleBroadcastServer::do_receive()
  {
    socket.async_receive_from(
        boost::asio::buffer(data, buffer_length),
        sender_endpoint,
        [this](boost::system::error_code ec, std::size_t bytes_recvd)
        {
//          dbg_perr("broadcaster: receive"
//              , "ip:", sender_endpoint.address()
//              , "port:", sender_endpoint.port());
          if (!ec && bytes_recvd > 0) {
            do_send(bytes_recvd);
          } else {
            do_receive();
          }
        }
    );
  }

  void SimpleBroadcastServer::do_send(std::size_t data_len)
  {
    bool responsed = false;

    if (not responsed) { // server replicator
      data[data_len] = '\0';
      boost::regex regex(SERVER_HELLO_MESSAGE " ID (\\d+) REPLICATOR\n");
      boost::cmatch match;
      if (boost::regex_match(data, match, regex)) {
        auto sender_id = std::stoll(match[1]);
        if (sender_id != broadcaster->model_id()) {
          socket.async_send_to(
              boost::asio::buffer(hello_message().c_str(), hello_message().length()),
              sender_endpoint,
              [this](boost::system::error_code ec, std::size_t bytes_sent)
              {
                do_receive();
              }
          );
          responsed = true;
        }
      }
    }
    if (not responsed) { // client
      data[data_len] = '\0';
      boost::regex regex(CLIENT_HELLO_MESSAGE "\n");
      boost::cmatch match;
      if (boost::regex_match(data, match, regex)) {
        socket.async_send_to(
            boost::asio::buffer(hello_message().c_str(), hello_message().length()),
            sender_endpoint,
            [this](boost::system::error_code ec, std::size_t bytes_sent)
            {
              do_receive();
            }
        );
        responsed = true;
      }
    }
    if (not responsed) {
      do_receive();
    }
  }

  using BroadcastServer = SimpleBroadcastServer;

  /*
   * Broadcaster
   */

  class Broadcaster::Impl {
    public:
      short port;
      boost::asio::io_service io_service;
      std::unique_ptr<boost::asio::io_service::work> io_service_work;
      std::unique_ptr<std::thread> io_service_thread;

      BroadcastServer server;

      Ktulu::GameManagerPtr model;

      Impl(short port, std::string hello_message, Ktulu::GameManagerPtr model)
        : port(port)
          , server(io_service, port, hello_message)
          , model(model)
      {
      }
  };

  Broadcaster::Broadcaster(short port, const std::string hello_message, Ktulu::GameManagerPtr model)
    : pimpl(new Impl(port, hello_message, model))
  {
    pimpl->server.set_broadcaster(this);
  }

  Broadcaster::~Broadcaster()
  {
    close();
  }

  Ktulu::GameManager::id_type Broadcaster::model_id() const
  {
    return pimpl->model->id();
  }

  void Broadcaster::run()
  {
    dbg_perr("broadcaster: starting"
        , "hello_ message:", pimpl->server.hello_message());
    pimpl->io_service_work.reset(new boost::asio::io_service::work(pimpl->io_service));
    pimpl->io_service.run();
    dbg_perr("broadcaster: stopped");
  }

  void Broadcaster::start()
  {
    if (pimpl->io_service_thread.get() == NULL) {
      pimpl->io_service_thread.reset(new std::thread(&Broadcaster::run, this));
    }
  }

  void Broadcaster::close()
  {
    pimpl->io_service_work.reset();
    pimpl->io_service.stop();
    if (pimpl->io_service_thread.get() != NULL) {
      pimpl->io_service_thread->join();
      pimpl->io_service_thread.reset();
    }
  }
}
