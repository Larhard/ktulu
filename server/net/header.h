#include <memory>

namespace Server {
  class Broadcaster;
  class Replicator;
  using BroadcasterPtr = std::shared_ptr<Broadcaster>;
  using ReplicatorPtr = std::shared_ptr<Replicator>;
}
