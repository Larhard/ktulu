#ifndef SERVER_REPLICATOR_H
#define SERVER_REPLICATOR_H

#include <memory>
#include <string>
#include <vector>

#include "factory.h"
#include "header.h"
#include "ktulu_model/game.h"
#include "ktulu_model/game_manager.hpp"

namespace Server {
  struct PackedGame {
    Ktulu::game_id_t game_id;
    int day_no;
    int action_no;
    std::vector<Ktulu::player_id_t> players;

    bool decode(std::string data);
    std::string encode() const;
  };

  class Replicator : public Producible<Replicator> {
    private:
      class Impl;
      std::unique_ptr<Impl> pimpl;

      void init_commands();

    public:
      using timeout_t = int;

      Replicator(Ktulu::GameManagerPtr GameManager, timeout_t timeout, short port);
      ~Replicator();

      void run_watcher();
      void run_backuper();
      void start();
      void close();

      std::string exec(std::string command);
  };
}

#endif // SERVER_REPLICATOR_H
