#include "action.h"
#include "day.h"
#include "debug_print.h"

namespace Ktulu {
  void WhoreAction::execute(DayPtr day, GamePtr game)
  {
    Debug::dbg_perr("execute_action:", name(), "on:", day->name());
  }

  std::string WhoreAction::name() const
  {
    return "Whore Action";
  }

  void MinisterAction::execute(DayPtr day, GamePtr game)
  {
    Debug::dbg_perr("execute_action:", name(), "on:", day->name());
  }

  std::string MinisterAction::name() const
  {
    return "Minister Action";
  }
}
