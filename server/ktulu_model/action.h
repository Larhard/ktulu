#ifndef KTULU_MODEL_ACTION_H
#define KTULU_MODEL_ACTION_H

#include "header.h"

namespace Ktulu {
  class Action {
    private:
    public:
      virtual void execute(DayPtr day, GamePtr game) = 0;
      virtual std::string name() const = 0;
  };

  class WhoreAction : public Action {
    public:
      void execute(DayPtr day, GamePtr game);
      std::string name() const;
  };

  class MinisterAction : public Action {
    public:
      void execute(DayPtr day, GamePtr game);
      std::string name() const;
  };
}

#endif // KTULU_MODEL_ACTION_H
