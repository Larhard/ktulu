#ifndef KTULU_GAME_H
#define KTULU_GAME_H

#include <memory>

#include "factory.h"
#include "header.h"
#include "player.h"
#include "character.h"

namespace Ktulu {
    enum gstat_ {LOBBY, STARTED};
    class Game : public Producible<Game> {
        private:
	    std::map<std::string,std::unique_ptr<Character>> my_characters;
	    gstat_ status;
	    std::string my_name;
            class Impl;
            std::unique_ptr<Impl> pimpl;
	    std::function<void(std::string)> cancel_me;

	    void notify_all(std::string);

        public:
            using ChatMessagesContainer = std::vector<MessagePtr>;

	    Game(const PlayerPtr& owner, id_type id, std::string name);
            //Game(ModelPtr model);
            ~Game();

            void add_player(const PlayerPtr& player, id_type id);
            int get_players_number() const;
            id_type id() const;
			std::string get_name();
            void start();
			gstat_ get_status();

            void post_chat_message(MessagePtr message);
            ChatMessagesContainer get_chat_messages() const;

            void set_day(DayPtr new_day);
            void next_step();

		void set_cancel_me(std::function<void(std::string)> func);

            class InProgressError : public std::runtime_error {
                public:
                    InProgressError() : runtime_error("Game in progress") {}
                    InProgressError(std::string msg) : runtime_error(msg) {}
            };
    };
}

#endif // KTULU_GAME_H
