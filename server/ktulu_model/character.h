#include <string>

#include "header.h"

#ifndef KTULU_CHARACTER

#define KTULU_CHARACTER

enum character_type_ {INDIAN, CITIZEN, BANDIT, UFO, NIGGA, BUNGLER_IDOL};

namespace Ktulu {

	class Character {
		public:
			Character(std::string name,character_type_ character_type_,std::string description);
			std::string get_name();

			void set_player(id_type player);
			id_type get_player();

			character_type_ get_type();
			std::string get_description();
		private:
			std::string name;
			id_type player;
			character_type_ my_type;
			std::string description;
	};

}

#endif
