#include <boost/regex.hpp>

#include "player.h"

namespace Ktulu {

	Player::~Player()
	{
	}
	Player::Player(boost::asio::io_service& io_service, id_type id)
		: my_socket(io_service), my_io_service(io_service)
	{
		this->name = "UNNAMED\n";
		this->own_command_map = get_command_map();	
		this->my_id = id;

		// SET_NAME
		this->own_command_map["SET_NAME"].set_on_command_run_function(
		[this](std::string s){
			std::string return_message;
			if(this->server_status != UNNAMED) {
				return_message = "ERR 6 ###";
			}
			else {
				boost::regex name_regex("[A-Za-z0-9_]*");
				boost::cmatch name_match;
				boost::regex_search(s.c_str(), name_match, name_regex);
				std::string name_ = name_match[0];

				this->name = name_;
				this->server_status = FREE;

				return_message = "OK SET_NAME " + name_ + " ###";

				std::cerr << return_message << std::endl;
			}
			return return_message;
		});

		//LIST_GAMES

		this->own_command_map["LIST_GAMES"].set_on_command_run_function(
		[this](std::string s){
			std::string return_message;

			if(this->server_status != FREE) {
				return_message = "ERR 7 ###";
			}
			else {
				return_message = "OK LIST_GAMES " + list_games_function() + " ###";
			}

			return return_message;
		});

		//CREATE_GAME

		this->own_command_map["CREATE_GAME"].set_on_command_run_function(
		[this](std::string s){
			std::string return_message;
			if(this->server_status != FREE) {
				return_message = "ERR 7 ###";
			}
			else {
				boost::regex name_regex("[A-Za-z0-9_]*");
				boost::cmatch name_match;
				boost::regex_search(s.c_str(), name_match, name_regex);
				std::string new_game_name = name_match[0];

				int result = create_game_function(this->get_id(),new_game_name);

				if(result == 1) {
					return_message = "ERR 8 ###";
				}
				else {
					this->server_status = LOBBY_OWNER;

					return_message = "OK CREATE_GAME " + new_game_name + " ###";
				}
			}

			return return_message;
		});

		//JOIN_GAME

		this->own_command_map["JOIN_GAME"].set_on_command_run_function(
		[this](std::string s){
			std::string return_message;

			if(this->server_status != FREE) {
				return_message = "ERR 7 ###";
			}
			else {
				boost::regex name_regex("[A-Za-z0-9_]*");
				boost::cmatch name_match;
				boost::regex_search(s.c_str(), name_match, name_regex);
				std::string game_name = name_match[0];

				int result = join_game_function(this->get_id(),game_name);
				if(result == 1) {
					return_message = "ERR 9 ###";
				}
				else {
					this->server_status = IN_LOBBY;
					return_message = "OK JOIN_GAME " + game_name + " ###";
				}
			}

			return return_message;
		});

		//LIST_PLAYERS

		this->own_command_map["LIST_PLAYERS"].set_on_command_run_function(
		[this](std::string s){
			std::string return_message;
			if(this->server_status != IN_LOBBY && this->server_status != LOBBY_OWNER && this->server_status != IN_GAME) {
				return_message = "ERR 10 ###";
			}
			else {
				std::cerr << "Jestem przed\n";
				std::string list_of_players = this->list_players_function();
				std::cerr << "Jestem po\n";
				return_message = "OK LIST_PLAYERS " + list_of_players + " ###";
			}

			return return_message;
		});

		//LEAVE

		this->own_command_map["LEAVE"].set_on_command_run_function(
		[this](std::string s) {
			std::string return_message;

			if(this->server_status != IN_LOBBY) {
				return_message = "ERR 11 ###";
			}
			else {
				leave_function(this->get_id());
				this->server_status = FREE;
				return_message = "OK LEAVE ###";
			}
			return return_message;
		});

		//CANCEL

		this->own_command_map["CANCEL"].set_on_command_run_function(
		[this](std::string s){
			std::string return_message;

			if(this->server_status != LOBBY_OWNER) {
				return_message = "ERR 12 ###";
			}
			else {
				cancel_function();
				std::cerr << "To jeszcze nie tu!?";
				this->server_status = FREE;
				return_message = "OK CANCEL ###";
			}

			return return_message;
		});

		//START

		this->own_command_map["START"].set_on_command_run_function(
		[this](std::string s){
			std::string return_message;
			
			if(this->server_status != LOBBY_OWNER) {
				return_message = "ERR 12 ###";
			}
			else {
				int result = start_function();
				this->server_status = IN_GAME;
				return_message = "OK START ###";
			}
			return return_message;
		});

		//CHAT

		this->own_command_map["CHAT"].set_on_command_run_function(
		[this](std::string s){
			std::string return_message;
			
			if(this->server_status != LOBBY_OWNER && server_status != IN_GAME && server_status != IN_LOBBY) {
				return_message = "ERR 12 ###";
			}
			else {
				s = s.substr(0, s.size());
				chat_function(this->get_name() + " :: " + s);
				return_message = "OK CHAT ###";
			}
			return return_message;
		});
	}
}
