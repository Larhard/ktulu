#include "game_manager.hpp"

namespace Ktulu {

GameManager::GameManager(boost::asio::io_service& io_service, short port)
	: my_io_service(io_service),
	  my_acceptor(io_service, tcp::endpoint(tcp::v4(), port))
{
	std::srand(std::time(0));
	my_id = std::rand();
}

void GameManager::start() {
	start_accept();
}

GameManager::~GameManager()
{
}

void GameManager::start_accept()
{
	player_id_t new_player_id;
	do{
		std::srand(std::time(0));
		new_player_id = rand();}
	while (my_players.count(new_player_id) != 0);

	my_players[new_player_id] = std::shared_ptr<Player>(new Player(my_io_service, new_player_id));

	
	my_acceptor.async_accept(my_players[new_player_id]->get_socket(),
		[this,new_player_id](const boost::system::error_code err_code) {
		std::cerr << "Złapłem!!!\n";
			if (!err_code)
			{
				my_players[new_player_id]->set_list_games(
					[this]() {
						std::string result("Actually joinable games:\n");
						for(auto g : my_games) {
							result += (g.second->get_name() + "\n");
						}
						return result;
					}	
				);

				my_players[new_player_id]->set_join_game(
					[this](player_id_t p,std::string game_name) {
						if(!my_games.count(game_name)) return 1;
						my_games[game_name]->add_player(my_players[p], p);
						return 0;
					}	
				);

				my_players[new_player_id]->set_create_game(
					[this](player_id_t p,std::string game_name) {
						if(my_games.count(game_name)) return 1;
						std::cerr << p << std::endl;
						std::cerr << my_players[p]->get_name() << std::endl;
						my_games[game_name] = std::shared_ptr<Game>(new Game(my_players[p],p,game_name));
						my_games[game_name]->set_cancel_me(
							[this](std::string name) {
								my_games.erase(name);
							}
						);
						return 0;
					}
				);

				/*my_players[new_player_id]->server_chat_message(
					[this](player * p,std::string game_name) {
						for(std::map<std::string,player> iter = players.begin();
						    iter != players.end(); iter++) {
							if(iter->first != name && iter->second->get_stat() != FREE) {
								iter->second->send_message(
									p->get_name()+": "+message);
							}
						}
						return ;
					}
				);*/
				my_players[new_player_id]->start();

				std::cerr << "Są calle, zaczynam nasłuch\n";
				start_accept();
			}
			else
			{
				my_players.erase(new_player_id);
			}
		}
	);	
				//boost::bind(&server::handle_accept, this, new_player,
				//boost::asio::placeholders::error));
}

/*void handle_accept(player* new_player,
		const boost::system::error_code& error) {
	if (!error)
	{
		new_player->set_list_games(
			[this]() {
				std::string result("Actually joinable games:\n");
				for(auto g : my_games) {
					result += (g.second->get_name() + "\n");
				}
				return result;
			}	
		);
		new_player->set_join_game(
			[this](player * p,std::string game_name) {
				if(!my_games.count(game_name)) return (game*) NULL;
				my_games[game_name]->add_player(p);
				return my_games[game_name];
			}	
		);
		new_player->set_create_game(
			[this](player * p,std::string game_name) {
				if(my_games.count(game_name)) return (game*) NULL;
				game * new_game = new game(p,game_name);
				my_games[game_name] = new_game;
				return new_game;
			}
		);
		new_player->server_chat_message(
			[this](player * p,std::string game_name) {
				for(std::map<std::string,player> iter = players.begin();
				    iter != players.end(); iter++) {
					if(iter->first != name && iter->second->get_stat() != FREE) {
						iter->second->send_message(
							p->get_name()+": "+message);
					}
				}
				return ;
			}
		);
		new_player->start();
	}
	else
	{
		delete new_player;
	}

	start_accept();
}*/

GameManager::id_type GameManager::id() {
	return this->my_id;
}

}
