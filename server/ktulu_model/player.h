#ifndef KTULU_PLAYER_H
#define KTULU_PLAYER_H

#include <memory>
#include <boost/asio.hpp>
#include <regex>
#include <functional>

#include "factory.h"
#include "header.h"
#include "common/ktulu_protocol/command.hpp"
#include "common/ktulu_protocol/some_useful_sets.hpp"

using boost::asio::ip::tcp;

namespace Ktulu {
	class Player : public Producible<Player> {
		private:
			tcp::socket my_socket;
			boost::asio::io_service& my_io_service;

			id_type my_id;
			std::string name;
			char buffer[2048];
			boost::asio::streambuf strange_buff;
			std::map<std::string,command> own_command_map;

			//------ When setting name - server

			std::function<void(std::string)> set_name_function;

			//------ When free - server

			std::function<std::string(void)> list_games_function;
			std::function<int(int,std::string)> join_game_function;
			std::function<int(int,std::string)> create_game_function;

			//------ In lobby

			std::function<std::string(void)> list_players_function;
			std::function<void(id_type)> leave_function;
			std::function<void(void)> cancel_function;
			std::function<int(void)> start_function;
			std::function<void(std::string)> chat_function;

			//------ In game

			std::function<void(std::string)> choose_player_function;
			std::function<void(std::string)> duel_function;

			server_status_ server_status;
			game_status_ game_status;

			void do_send(std::string to_send);
			std::string do_process(std::string to_process);

		public:
			Player(std::string name); //-- cannot set name before connecting with socket
			Player(boost::asio::io_service& io_service, id_type id);
			~Player();

			int get_id() const;
			std::string get_name() const;

			void set_set_name(std::function<int(std::string)> func); 
			void set_list_games(std::function<std::string(void)> func);
			void set_join_game(std::function<int(int,std::string)> func);
			void set_create_game(std::function<int(int,std::string)> func);
			void set_cancel(std::function<int(void)> func);
			void set_leave(std::function<void(id_type)> func);
			void set_start(std::function<int(void)> func);
			void set_list_players(std::function<std::string(void)> func);
			void set_chat(std::function<void(std::string)> func);
			void set_choose_player(std::function<int(std::string)> func);
			void set_duel(std::function<int(std::string)> func);

			// session stuff

			tcp::socket& get_socket();

			void start();
			void notify(std::string message);
			void wake_up();
			void sleep();

			void game_cancelled();
			void game_started(std::string);
	};
}

#endif // KTULU_PLAYER_H
