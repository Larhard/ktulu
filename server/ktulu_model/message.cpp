#include "message.h"
#include <string>

namespace Ktulu {

    //Pimpl

    class Message::Impl {
        private:
        public:
            const PlayerPtr sender;
            const std::string body;

            Impl(PlayerPtr sender, std::string body);
    };

    Message::Impl::Impl(PlayerPtr sender, std::string body) : sender(sender), body(body) {}

    // Methods

    Message::Message(PlayerPtr sender, std::string body) : pimpl(new Impl(sender, body)) {}

    Message::~Message()  {}

    PlayerPtr Message::get_sender() const {
        return pimpl->sender;
    }
    std::string Message::get_body() const {
        return pimpl->body;
    }

    std::ostream & operator<<(std::ostream &stream, const Message &message) {
        stream << "Message from : " << *(message.get_sender());
        return stream;
    }
}
