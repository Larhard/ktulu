#include <iostream>
#include <memory>
#include <vector>
#include <string>

namespace Ktulu {
  class Action;
  class Day;
  class Game;
  class Message;
  class GameManager;
  class Player;
  std::ostream & operator<<(std::ostream &stream, const Game &game);
  std::ostream & operator<<(std::ostream &stream, const Message &message);
  std::ostream & operator<<(std::ostream &stream, const Player &player);
  using ActionPtr = std::shared_ptr<Action>;
  using DayPtr = std::shared_ptr<Day>;
  using GamePtr = std::shared_ptr<Game>;
  using MessagePtr = std::shared_ptr<Message>;
  using GameManagerPtr = std::shared_ptr<GameManager>;
  using PlayerPtr = std::shared_ptr<Player>;
  typedef unsigned int player_id_t;
  typedef unsigned int game_id_t;
  typedef unsigned int id_type;
}

