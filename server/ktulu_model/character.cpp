#include "character.h"

namespace Ktulu {
	Character::Character(std::string name, character_type_ type, std::string description) {
		this->name = name;
		this->my_type = type;
		this->description = description;
	}

	std::string Character::get_name() {
		return this->name;
	}

	std::string Character::get_description() {
		return this->description;
	}

	void Character::set_player(id_type player)  {
		this->player = player;
	}

	id_type Character::get_player() {
		return this->player;
	}

	character_type_ Character::get_type() {
		return this->my_type;
	}
}
