#include <boost/regex.hpp>
#include <unistd.h>

#include "player.h"

namespace Ktulu {

	void Player::start()
	{
//		boost::asio::basic_streambuf<std::allocator> strange_buff;
		std::cerr << "Started reading\n";
		strange_buff.consume(strange_buff.size());
		async_read_until(
			my_socket,
			strange_buff,
			"###",
			[this](boost::system::error_code err_code, std::size_t recieved){
				std::cerr << "Reading handler\n";
				std::cerr << recieved << std::endl;
				std::cerr << strange_buff.size() << std::endl;

				strange_buff.sgetn(buffer, recieved);
				std::string to_process(buffer);
				std::string result = do_process(to_process);

				std::cerr << "My answer: " << result << std::endl;

				my_socket.async_write_some(
					boost::asio::buffer(result.c_str(), result.size()),
					[this](const boost::system::error_code err_code, std::size_t bt) {
						std::cerr << "Writing handler\n";
						if(!err_code) {
							start();
						}
						else {
							//tu możemy szykować przełączenie serwera
						}	
					}
				);
			}
		);
	}

	void Player::sleep() {
		std::string message = "SLEEP ###";
		this->do_send(message);
	}
	
	void Player::wake_up() {
		std::string message = "WAKE_UP ###";
		this->do_send(message);
	}

	void Player::notify(std::string message) {
		message = "NOTIFY " + message + " ###";
		std::cerr << "MESSAGE: " << message << std::endl;
		this->do_send(message);
	} 

	void Player::do_send(std::string to_send) {
		my_socket.async_write_some(
			boost::asio::buffer(to_send.c_str(), to_send.size()),
			[](boost::system::error_code ec, std::size_t cw) {
				if(ec) {
					std::cerr << ec << "Wypisałem : " << cw << std::endl;
					std::cerr << "Some errors detected.\n";
				}
			}
		);
	}

std::string Player::do_process(std::string to_process) {
		to_process = to_process + "#";
		boost::regex command_regex("[A-Z_]*");
		boost::cmatch command_match;
		boost::regex_search(to_process.c_str(), command_match, command_regex);
		std::string command_match_result = command_match[0];
		std::cout << "Znaleziona komenda: " << command_match_result << std::endl;

		to_process = to_process.substr(command_match_result.size()+1);
		boost::regex com_reg("[^#]*");
		boost::smatch match;
		boost::regex_search(to_process,match,com_reg);
		to_process = match[0];

		std::cerr << "Dane: " << to_process << std::endl;

		if(own_command_map.count(command_match_result) == 0) {
			return "ERROR 1 ###";
		}
		else {
			return own_command_map[command_match_result].run_me(to_process);
		}
	}

	tcp::socket& Player::get_socket() {
		return this->my_socket;
	}

	    void Player::game_cancelled() {
		this->server_status = FREE;
		this->notify("Your game was cancelled.");
	    }

	    void Player::game_started(std::string my_card) {
	    	this->server_status = IN_GAME;
		this->notify("Your game's just started!\n" + my_card);
	    }
}

