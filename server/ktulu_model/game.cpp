#include <atomic>
#include <cassert>

#include "day.h"
#include "debug_print.h"
#include "game.h"

namespace Ktulu {

    // PImpl

    class Game::Impl {
        private:
            static std::atomic<id_type> global_id;

        public:
            ChatMessagesContainer messages;

            bool in_progress;
            id_type id;

            static id_type get_unique_id();
            DayPtr day;

            std::map<id_type,PlayerPtr> my_players;
    };

    id_type Game::Impl::get_unique_id() {  // TODO make id globally unqie
        assert(global_id);
        return global_id++;
    }

    std::atomic<id_type> Game::Impl::global_id (1);


    // Methods

/*    Game::Game(ModelPtr model) : pimpl(new Impl()) {
        pimpl->id = model->id() + Impl::get_std::unique_id();
        pimpl->in_progress = false;
    }*/

    Game::Game(const PlayerPtr& p, id_type id, std::string name) : pimpl(new Impl()) {
	std::cerr << "Konstrunktor game\n";
	this->my_name = name;
	std::cerr << p->get_name();
	this->add_player(p, id);

	// tu będziemy tworzyć charactery

    }

    Game::~Game() {}

    void Game::add_player(const PlayerPtr& player, id_type identifier) {
	std::cerr << "Oto i dodawanie gracza\n";
        if (pimpl->in_progress) {
            throw InProgressError();
        }
	std::cerr << "Teraz będziemy dodawać. Czyżbyśmy nie konstruowali pimpla?\n";
	pimpl->my_players[identifier] = PlayerPtr(player);
	std::cerr << "Something\n";
	pimpl->my_players[identifier]->set_cancel([this](){
		for(auto p : pimpl->my_players) {
			p.second->game_cancelled();
			pimpl->my_players.erase(p.first);
		}
		cancel_me(this->get_name());
		return 1;
	});
	pimpl->my_players[identifier]->set_leave([this](id_type id){
		pimpl->my_players.erase(id);
		return ;
	});
	pimpl->my_players[identifier]->set_list_players([this](){
		std::string result = "";
		std::cerr << "Jestem tu\n";
		for(auto p : pimpl->my_players) {
			result = result + p.second->get_name() + "\n";
		}
		return result;
	});
	pimpl->my_players[identifier]->set_start([this](){
		this->start();
		return 1;
	});
	pimpl->my_players[identifier]->set_chat(
		[this](std::string message){
			this->notify_all(message);
			return "sent";
		}
	);
    }

    void Game::set_cancel_me(std::function<void(std::string)> func) {
    	this->cancel_me = func;
    }

    id_type Game::id() const {
        return pimpl->id;
    }

    int Game::get_players_number() const {
        return pimpl->my_players.size();
    }

    std::vector<std::string> shuffle() {
    	
    }
    void Game::start() {
        pimpl->in_progress = true;
	std::vector<std::string> rand_vect;
	my_characters["WHORE"] = std::unique_ptr<Character>(new Character(
		"WHORE",
		CITIZEN,
		"Sleeps with somebody first night, checking his card. Probably strongest character in game."
	));
	rand_vect.push_back("WHORE");
	my_characters["SHERIFF"] = std::unique_ptr<Character>(new Character(
		"SHERIFF",
		CITIZEN,
		"Every night sends somebody to the jail. When he is alive you can refuse the duel."
	));
	rand_vect.push_back("SHERIFF");
	my_characters["DRUNKEN JUDGE"] = std::unique_ptr<Character>(new Character(
		"DRUNKEN JUDGE",
		CITIZEN,
		"Every night is drinking with somebody, this person don't wake up this night. Can one time change result of duel."
	));
	rand_vect.push_back("DRUNKEN JUDGE");
	my_characters["PASTOR"] = std::unique_ptr<Character>(new Character(
		"PASTOR",
		CITIZEN,
		"Every night checks type of one of the characters."
	));
	rand_vect.push_back("PASTOR");
	my_characters["GOOD GUNSLINGER"] = std::unique_ptr<Character>(new Character(
		"GOOD GUNSLINGER",
		CITIZEN,
		"Beats everybody in duel. Exception can be drunken judge or bad gunslinger."
	));
	rand_vect.push_back("GOOD GUNSLINGER");
	my_characters["BLACKMAILER"] = std::unique_ptr<Character>(new Character(
		"BLACKMAILER",
		BANDIT,
		"On the begin blackmails somebody. This person cannot plays against him.."
	));
	rand_vect.push_back("BLACKMAILER");
	my_characters["THIEF"] = std::unique_ptr<Character>(new Character(
		"THIEF",
		BANDIT,
		"One time in game can try to steal the precious."
	));
	rand_vect.push_back("THIEF");
	my_characters["BAD GUNSLINGER"] = std::unique_ptr<Character>(new Character(
		"BAD GUNSLINGER",
		BANDIT,
		"Beats everybody in duel. Exception can be drunken judge or good gunslinger."
	));
	rand_vect.push_back("BAD GUNSLINGER");
	my_characters["MURDERER"] = std::unique_ptr<Character>(new Character(
		"MURDERER",
		BANDIT,
		"One time per game can murder somebody."
	));
	rand_vect.push_back("MURDERER");
	my_characters["SHAMAN"] = std::unique_ptr<Character>(new Character(
		"SHAMAN",
		INDIAN,
		"One time per night can check if a person has the precious."
	));
	rand_vect.push_back("SHAMAN");
	my_characters["SHAMANESS"] = std::unique_ptr<Character>(new Character(
		"SHAMANESS",
		INDIAN,
		"One time per game can apply somebody deadly venom."
	));
	rand_vect.push_back("SHAMANESS");
	my_characters["HAWK EYE"] = std::unique_ptr<Character>(new Character(
		"HAWK EYE",
		INDIAN,
		"One time in game, after indian time, can check who has the precious."
	));
	rand_vect.push_back("HAWK EYE");
	my_characters["SILENT FOOT"] = std::unique_ptr<Character>(new Character(
		"SILENT FOOT",
		INDIAN,
		"Can give precious to somebody and this person doesn't know that he has it."
	));
	rand_vect.push_back("SILENT FOOT");
	my_characters["FLAMING FURY"] = std::unique_ptr<Character>(new Character(
		"FLAMING FURY",
		INDIAN,
		"When he is last indian he gets additional kill."
	));
	rand_vect.push_back("FLAMING FURY");

	random_shuffle(rand_vect.begin(), rand_vect.end(), 
		[&rand_vect](int i){std::srand(std::time(0)); return std::rand()%rand_vect.size();});
	int i = 0;

	for(auto p : pimpl->my_players) {
		my_characters[rand_vect[i]]->set_player(p.first);
		p.second->game_started(my_characters[rand_vect[i]]->get_name() + " - " 
			+ my_characters[rand_vect[i]]->get_description());
		i++;
	}

        Debug::dbg_perr("game: start players_count:", pimpl->my_players.size());
        set_day(Factory<StartingDay>::create());
    }

    void Game::post_chat_message(MessagePtr message) {
        std::cerr << *message << std::endl;
        pimpl->messages.push_back(message);
    }

    Game::ChatMessagesContainer Game::get_chat_messages() const {
        return pimpl->messages;
    }

    void Game::next_step()
    {
        pimpl->day->execute_next(self());
    }

    void Game::set_day(DayPtr new_day)
    {
        pimpl->day = new_day;
    }

    std::string Game::get_name() {
    	return this->my_name;
    }

    std::ostream & operator<<(std::ostream &stream, const Game &message) {
        stream << "Game #" << message.id();
        return stream;
    }

    gstat_ Game::get_status() {
    	return this->status;
    }

    void Game::notify_all(std::string message) {
    	for(auto p : pimpl->my_players) {
		p.second->notify(message);
	}
    }
}
