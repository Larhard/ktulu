#include "player.h"

namespace Ktulu {

	int Player::get_id() const {
		return my_id;
	}

	std::string Player::get_name() const {
		return name;
	}
	std::ostream& operator<< (std::ostream& ostr, Player const& player) {
		ostr << "Gracz: " << player.get_name() << " " << player.get_id() << std::endl;
		return ostr;
	}
}
