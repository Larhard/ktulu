#include <string>
#include <cstdlib>
#include <boost/asio.hpp>
#include <boost/system/error_code.hpp>
#include <ctime>
#include <mutex>

#include "header.h"
#include "player.h"
#include "game.h"


#ifndef KTULU_GAME_MANAGER

#define KTULU_GAME_MANAGER

using boost::asio::ip::tcp;

namespace Ktulu {
	class GameManager
	{
		public:
			using id_type = std::size_t;

			GameManager(boost::asio::io_service& io_service, short port);
			~GameManager();

			void start();

			id_type id();
		private:
			void start_accept();
		//	void handle_accept(player_id_t new_player, const boost::system::error_code& error);

			boost::asio::io_service& my_io_service;
			tcp::acceptor my_acceptor;
			id_type my_id;
			std::mutex players_mutex;
			std::mutex games_mutex;

			std::map<std::string,GamePtr> my_games;
			std::map<player_id_t,PlayerPtr> my_players;
	};
}

#endif
