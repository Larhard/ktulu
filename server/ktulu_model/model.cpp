#include <atomic>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <mutex>

#include "debug_print.h"
#include "game.h"
#include "model.h"

namespace Ktulu {
    class Model::Impl {
        public:
            id_t id;
            std::vector<GamePtr> games;
            mutable std::mutex games_mutex;
    };

    Model::Model() : pimpl(new Impl()) {
        std::srand(std::time(NULL));
        pimpl->id = std::rand();
        Debug::dbg_perr("model: create id:", id());
    }

    Model::~Model() {}

    GamePtr Model::create_new_game() {
        std::lock_guard<std::mutex> games_lock(pimpl->games_mutex);

        GamePtr new_game = create<Game>(self());
        Debug::dbg_perr("new_game:", new_game->id());
        pimpl->games.push_back(new_game);
        return new_game;
    }

    GamePtr Model::get_game_by_id(const Game::id_t id) const {
        std::lock_guard<std::mutex> games_lock(pimpl->games_mutex);

        for (auto game : pimpl->games) {
            if (game->id() == id) {
                return game;
            }
        }
        return GamePtr(NULL);
    }

    Model::id_t Model::id() const
    {
        return pimpl->id;
    }
}
