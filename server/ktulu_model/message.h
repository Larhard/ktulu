#ifndef KTULU_MESSAGE_H
#define KTULU_MESSAGE_H

#include <iostream>
#include <memory>

#include "header.h"
#include "player.h"

namespace Ktulu {
    class Message {
        private:
            class Impl;
            std::unique_ptr<Impl> pimpl;
        public:
            Message(PlayerPtr player, std::string message);
            ~Message();

            PlayerPtr get_sender() const;
            std::string get_body() const;
    };
}

#endif // KTULU_MESSAGE_H
