#include <string>

#include "action.h"
#include "day.h"
#include "debug_print.h"

namespace Ktulu {
  DayPtr Day::self() const
  {
    return _self.lock();
  }

  /*
   * Custom Days
   */
  ZeroNight::ZeroNight()
  {
    actions.push_back(ActionPtr(new WhoreAction()));
    actions.push_back(ActionPtr(new MinisterAction()));
  }

  std::string ZeroNight::name() const
  {
    return "zero night";
  }
}
