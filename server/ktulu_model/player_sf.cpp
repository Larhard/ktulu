#include "player.h"

namespace Ktulu {

	// setting external functions

	//------ setting name setting function

	void Player::set_set_name(std::function<int(std::string)> func) {this->set_name_function = func;}

	//------ setting free player function

	void Player::set_list_games(std::function<std::string(void)> func) {this->list_games_function = func;}
	void Player::set_join_game(std::function<int(int,std::string)> func) {this->join_game_function = func;}
	void Player::set_create_game(std::function<int(int,std::string)> func) {this->create_game_function = func;}

	//------ setting lobby functions

	void Player::set_cancel(std::function<int(void)> func) {this->cancel_function = func;}
	void Player::set_leave(std::function<void(id_type)> func) {this->leave_function = func;}
	void Player::set_start(std::function<int(void)> func) {this->start_function = func;}
	void Player::set_list_players(std::function<std::string(void)> func) {this->list_players_function = func;}
	void Player::set_chat(std::function<void(std::string)> func) {this->chat_function = func;}

	//------ setting game functions

	void Player::set_choose_player(std::function<int(std::string)> func) {this->choose_player_function = func;}
	void Player::set_duel(std::function<int(std::string)> func) {this->duel_function = func;}
}
