#ifndef KTULU_MODEL_DAY_H
#define KTULU_MODEL_DAY_H

#include <cassert>

#include "header.h"
#include "action.h"
#include "debug_print.h"
#include "factory.h"
#include "game.h"

/*
 * implements full cycle of the game (
 */
namespace Ktulu {
  class Day {
    private:
    public:
    std::weak_ptr<Day> _self;
      DayPtr self() const;

      virtual void execute_next(GamePtr game) = 0;
      virtual std::string name() const = 0;
  };

  template<typename NextDay>
    class StdDay : public Day {
      protected:
        std::vector<ActionPtr> actions;
        int action_idx;
      public:
        StdDay()
          : action_idx(0)
        {
        }

        virtual void execute_next(GamePtr game)
        {
          assert(game.get() != NULL);
          assert(action_idx < actions.size());
          Debug::dbg_perr("day:", name(), "execute next action", action_idx
              , ":", actions[action_idx]->name());
          actions[action_idx]->execute(self(), game);
          ++action_idx;
          if (action_idx == actions.size()) {
            Debug::dbg_perr("day:", name(), "new day");
            game->set_day(Factory<NextDay>::create());
          }
        }

        virtual std::string name() const = 0;
    };

  /*
   * Custom Days
   */
  class ZeroNight : public StdDay<ZeroNight> {
    public:
      ZeroNight();
      std::string name() const;
  };

  using DayPtr = std::shared_ptr<Day>;
  using StartingDay = ZeroNight;
}

#endif // KTULU_MODEL_DAY_H
