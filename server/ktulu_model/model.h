#ifndef KTULU_MODEL_H
#define KTULU_MODEL_H

#include <memory>

#include "factory.h"
#include "game.h"
#include "header.h"

namespace Ktulu {
    class Model : public Producible<Model> {
        private:
            class Impl;
            std::unique_ptr<Impl> pimpl;

        public:
            using id_t = std::size_t;

            Model();
            ~Model();

            GamePtr get_game_by_id(const Game::id_t id) const;
            GamePtr create_new_game();
            id_t id() const;
    };
}

#endif // KTULU_MODEL_H
