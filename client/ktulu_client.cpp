#include <boost/regex.hpp>

#include "ktulu_client.h"

namespace Ktulu {
	std::string Client::interpret_server_answer(std::string ans) {
		ans = ans.substr(0, ans.size()-4)+"\n";
		if(ans.substr(0,6) == "NOTIFY") {
			ans = ans.substr(7);
			return ans;
		}
		else if(ans.substr(0,2) == "OK") {
			ans = ans.substr(3);
			boost::regex command_regex("[A-Z_]*");
			boost::cmatch com_match;
			boost::regex_search(ans.c_str(), com_match, command_regex);
			std::string res = com_match[0];

			if(own_command_map.count(res) == 0) {
			       return "LOCAL ERROR: Unknown return command.\n";
			}
			else {
				return own_command_map[res].interpret_my_answer(ans.substr(res.size()+1));
			}
		}
		else if(ans.substr(0,3) == "ERR") {
			std::stringstream ss(ans.substr(4));
			int parsed;
			ss >> parsed;

			if(parsed >= MIN_ERROR_CODE && parsed <= MAX_ERROR_CODE) {
				return "SERVER ERROR: " + ktulu_error_code[parsed];
			}
			else {
				return "SERVER ERROR: " + ktulu_error_code[UNKNOWN_ERROR_MESSAGE];
			}
		}
		else {
		}
	}
	void Client::write(std::string command) {
		//tu będzie obsługa
		if(my_status == UNNAMED) {
			bool proper = true;
			for(int i = 0; i<command.size(); i++) {
				if(!(command[i] == '_' || ('a'<=command[i] && command[i]<='z') 
						|| ('A'<=command[i] && command[i]<='Z') || 
							('0'<=command[i] && command[i]<='9'))) {
					proper = false;
					break;
				}
			}
			if(!proper) {
				std::cout << "Wrong name.\n";
				std::cout << "Proper name contains only letters, numbers and underscore.\n";
				return ;
			}
			else {
				command = "SET_NAME " + command;
			}	
		} else {
		
		}
		command += "###";
		my_socket.async_write_some(boost::asio::buffer(command.c_str(), command.size()), 
			[this](boost::system::error_code ec, std::size_t bytes_transferred) {
				if(!ec) {
					write_handler();
				}
			});
	}
	void Client::close() {
		my_socket.close();
	}
	void Client::read() {
		boost::asio::async_read_until(my_socket, strange_buff, "###",
			[this](const boost::system::error_code err_code,
					std::size_t transferred) {
				if(!err_code) {
					strange_buff.sgetn(reading_buffer, transferred);
					reading_buffer[transferred] = '\0';
					std::string str(reading_buffer);
					std::cout << interpret_server_answer(str);
					read();
				}
				else {
					std::cerr << "PANIC: Lost connection with server.\n";
					exit(1);
				}
			}
		);
	}
	void Client::connect_handler() {
		read();
	}
	void Client::write_handler() {

	}

}
