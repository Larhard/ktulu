#include "ktulu_client.h"

namespace Ktulu {
	Client::Client(boost::asio::io_service& io_service, tcp::resolver::iterator itr) : 
		my_socket(io_service) , my_io_service(io_service) 
	{
		ktulu_error_code = get_ktulu_error_codes();
		own_command_map = get_command_map();
		
		own_command_map["SET_NAME"].set_answer_interpretation (
			[this](std::string) {
				this->my_status = FREE;
				return "Now you joined our game room.\n Type HELP for more info.\n";
			}
		);

		own_command_map["CREATE_GAME"].set_answer_interpretation (
			[this](std::string) {
				this->my_status = LOBBY_OWNER;
				return "Your game was succesfully created.\n";
			}
		);

		own_command_map["JOIN_GAME"].set_answer_interpretation (
			[this](std::string) {
				this->my_status = IN_LOBBY;
				return "You succesfully joined to game.\n";
			}
		);

		own_command_map["LIST_GAMES"].set_answer_interpretation (
			[this](std::string s) {
				return s;
			}
		);

		boost::asio::async_connect(my_socket, itr, 
			[this](boost::system::error_code ec, tcp::resolver::iterator itr) {
				if(!ec) connect_handler();
		});

		std::cout << "Welcome to Ktulu client v. 2.0\n";
		std::cout << "To be able to play, please enter your nick:\n";

	}
	Client::~Client()
	{
	}
}
