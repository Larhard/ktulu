#include <cstdlib>
#include <ctime>

#include "config.h"
#include "debug_print.h"
#include "ktulu_client.h"
#include "server_searcher.h"

int main(int argc, char **argv) {
    std::srand(std::time(NULL));

	std::cout << "Trying to connect to server ...\n";

	boost::asio::io_service io_service;

    auto servers = Ktulu::search_servers(DEFAULT_SEARCHER_PORT, 1, CLIENT_HELLO_MESSAGE "\n");
    if (servers.empty()) {
        std::cout << "No servers found" << std::endl;
        return 0;
    }
    Debug::dbg_perrall(std::begin(servers), std::end(servers));
    auto server = servers[std::rand() % servers.size()];
    std::cout << "Connecting to server " << server << std::endl;

	tcp::resolver resolver(io_service);
	tcp::resolver::query query(server.endpoint.address().to_string(), STR(DEFAULT_GAME_PORT));
	tcp::resolver::iterator iterator = resolver.resolve(query);

	Ktulu::Client client(io_service, iterator);

	std::thread reader_thread([&io_service](){io_service.run();});	

	while(1) {
		std::string command;
		getline(std::cin, command);
		client.write(command);
	}

	return 0;
}
