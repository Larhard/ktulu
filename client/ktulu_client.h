#include <iostream>
#include <map>
#include <vector>
#include <string>
#include <thread>
#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <boost/system/error_code.hpp>

#include "common/ktulu_protocol/some_useful_sets.hpp"

using boost::asio::ip::tcp;

namespace Ktulu {
	class Client {
		public:
			Client(boost::asio::io_service& io_service, tcp::resolver::iterator itr);
			~Client();

			void write(std::string command);
			void close();

		private:
			
			void read();
			void connect_handler();
			void write_handler();
			std::string interpret_server_answer(std::string ans);

			tcp::socket my_socket;
			boost::asio::io_service& my_io_service;

			std::vector<std::string> ktulu_error_code;
			std::map<std::string,command> own_command_map;

			server_status_ my_status = UNNAMED;
			boost::asio::streambuf strange_buff;
			char reading_buffer[2048];
			char buffer[1024];
	};
}
