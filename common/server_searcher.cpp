#include <boost/asio.hpp>
#include <boost/regex.hpp>
#include <iostream>
#include <string>
#include <thread>
#include <vector>

#include "config.h"
#include "server_searcher.h"
#include "utils.h"

using Debug::dbg_perr;
using boost::asio::ip::udp;

namespace Ktulu {
  enum {
    buffer_length = 1024
  };

  /*
   * ServerSearcher
   */
  class ServerSeracher::Impl {
    public:
      short port;
      std::string hello_message;
      char data[buffer_length];
      std::vector<ServerEndpoint> result;

      udp::endpoint remote_endpoint;

      boost::asio::io_service io_service;
      std::unique_ptr<boost::asio::io_service::work> io_service_work;
      udp::socket socket;

      boost::system::error_code ec;
      boost::regex regex;

      Impl(short port, const std::string &hello_message, const std::string &regex)
        : port(port)
          , hello_message(hello_message)
          , socket(io_service)
          , regex(regex)
      {
      }
  };

  ServerSeracher::ServerSeracher(short port, const std::string &hello_message, const std::string &regex)
    : pimpl(new Impl(port, hello_message, regex))
  {
  }

  ServerSeracher::~ServerSeracher()
  {
  }

  void ServerSeracher::do_receive()
  {
    pimpl->socket.async_receive_from(
        boost::asio::buffer(pimpl->data, buffer_length),
        pimpl->remote_endpoint,
        [this](boost::system::error_code ec, std::size_t bytes_recvd)
        {
          boost::cmatch match;
          if ((not ec) and boost::regex_match(pimpl->data, match, pimpl->regex)) {
//            dbg_perr("server_searcher: new server"
//                , "ip:", pimpl->remote_endpoint.address()
//                , "port:", pimpl->remote_endpoint.port());
            pimpl->result.push_back(ServerEndpoint{pimpl->remote_endpoint, pimpl->data});
          } else {
//            dbg_perr("server_searcher: invalid message"
//                , "ip:", pimpl->remote_endpoint.address()
//                , "port:", pimpl->remote_endpoint.port());
          }
          do_receive();
        }
    );
  }

  std::vector<ServerEndpoint> ServerSeracher::search(int timeout)
  {
    pimpl->socket.open(udp::v4(), pimpl->ec);
    if (!pimpl->ec) {
      pimpl->socket.set_option(udp::socket::reuse_address(true));
      pimpl->socket.set_option(boost::asio::socket_base::broadcast(true));
      udp::endpoint broadcast_endpoint(boost::asio::ip::address_v4::broadcast()
          , pimpl->port);
      pimpl->socket.send_to(boost::asio::buffer(pimpl->hello_message.c_str()
          , pimpl->hello_message.length())
          , broadcast_endpoint);

      char data[buffer_length];

      do_receive();

      std::thread io_service_thread([&](){
        pimpl->io_service_work.reset(new boost::asio::io_service::work(pimpl->io_service));
        pimpl->io_service.run();
      });

      std::this_thread::sleep_for (std::chrono::seconds(timeout));
      pimpl->io_service_work.reset();
      pimpl->io_service.stop();
      pimpl->socket.cancel();
      pimpl->socket.close();
      io_service_thread.join();
    }
    return pimpl->result;
  }

  /*
   * Shortcuts
   */
  std::vector<ServerEndpoint> search_servers(short port, int timeout, const std::string &hello_message, const std::string &regexp)
  {
    return Ktulu::ServerSeracher(port, hello_message, regexp).search(timeout);
  }

  /*
   * Streams
   */
  std::ostream & operator<<(std::ostream &stream, const ServerEndpoint &server_endpoint)
  {
    stream << server_endpoint.endpoint.address() << ":" << server_endpoint.endpoint.port();
    return stream;
  }
}
