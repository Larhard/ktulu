#include <string>
#include <vector>
#include <iostream>
#include <map>
#include <functional>

#include "command.hpp"

enum server_status_ {UNNAMED, FREE, IN_LOBBY, LOBBY_OWNER, IN_GAME};
enum game_status_ {VOTING, ON_ACTION, CHATTING, IN_JAIL, DRUNKEN, DEAD};


// error codes

#ifndef KTULU_ERROR_CODES

#define KTULU_ERROR_CODES

#define MAX_ERROR_CODE 10
#define MIN_ERROR_CODE 1
#define UNKNOWN_ERROR_MESSAGE 0

std::vector<std::string> get_ktulu_error_codes();

#endif

#ifndef PROTOCOL_COMMANDS_MAP

#define PROTOCOL_COMMANDS_MAP

std::map<std::string,command> get_command_map();

#endif
