#include "some_useful_sets.hpp"
#include "command.hpp"

std::map<std::string,command> get_command_map() {
	std::map<std::string, command> result;

	std::function<std::string(std::string)> defaultFunction = [](std::string s) {
		return s;
	};

// help
	command help_command(
		"HELP",
		"Shows client help.",
		CLIENT,
		defaultFunction,
		defaultFunction
	);
	result[help_command.get_name()] = help_command;

// set_name
	command set_name_command (
		"SET_NAME",
		"Sets players name.",
		CLIENT,
		defaultFunction,
		defaultFunction
	);
	result[set_name_command.get_name()] = set_name_command;

// create_game
	command create_game (
		"CREATE_GAME",
		"Creates new game.",
		CLIENT,
		defaultFunction,
		[](std::string x){return "Game created.";}
	);
	result[create_game.get_name()] = create_game;

// join_game
	command join_game (
		"JOIN_GAME",
		"Join existing game.",
		CLIENT,
		defaultFunction,
		defaultFunction
	);
	result[join_game.get_name()] = join_game;
// list_games
	command list_games (
		"LIST_GAMES",
		"Lists existing games.",
		CLIENT,
		defaultFunction,
		defaultFunction
	);
	result[list_games.get_name()] = list_games;
// list_players
	command list_players (
		"LIST_PLAYERS",
		"Lists players in your current game.",
		CLIENT,
		defaultFunction,
		defaultFunction
	);
	result[list_players.get_name()] = list_players;
// cancel game
	command cancel_game (
		"CANCEL",
		"Cancels game if lobby owner.",
		CLIENT,
		defaultFunction,
		defaultFunction
	);
	result[cancel_game.get_name()] = cancel_game;
// leave game
	command leave_game (
		"LEAVE",
		"Leaves game.",
		CLIENT,
		defaultFunction,
		defaultFunction
	);
	result[leave_game.get_name()] = leave_game;
// start game
	command start_game (
		"START",
		"Starts game.",
		CLIENT,
		defaultFunction,
		defaultFunction
	);
	result[start_game.get_name()] = start_game;
// chat
	command chat (
		"CHAT",
		"Sends message to all active players (in game to people from your game only).",
		CLIENT,
		defaultFunction,
		defaultFunction
	);
	result[chat.get_name()] = chat;

// choose player
	command choose_player (
		"CHOOSE_PLAYER",
		"Choose player in voting/action/sth else.",
		CLIENT,
		defaultFunction,
		defaultFunction
	);
	result[choose_player.get_name()] = choose_player;

// duel
	command duel (
		"DUEL",
		"Duel with other player.",
		CLIENT,
		defaultFunction,
		defaultFunction
	);
	result[duel.get_name()] = duel;

	return result;
}

// miejsce na funkcje wywoływane przez odpowiednie komendy

/*void helpFunction(ktuluClient client, string serverResponse, vector<string> localArguments)
{
	for(string s : client.getCommands().getKeySet()) {
		client.getOutputStream() << s;
		client.getOutputStream() << client.getCommands().getDescription();
		client.getOutputStream() << endl;
	}
}*/
