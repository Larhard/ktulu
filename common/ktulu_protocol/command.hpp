#include <string>
#include <functional>

#ifndef PROTOCOL_COMMANDS

#define PROTOCOL_COMMANDS

enum _owner {CLIENT, SERVER};

class command {
	public:
		command();
		command(std::string name,
			std::string description,
			_owner owner,
			std::function<std::string(std::string)> on_command_run,
			std::function<std::string(std::string)> answer_interpretation
		);

		std::string get_name();
		std::string get_description();
		_owner get_owner();

		std::string run_me(std::string arguments);
		std::string interpret_my_answer(std::string answer); 		

		void set_on_command_run_function(std::function<std::string(std::string)> func);
		void set_answer_interpretation(std::function<std::string(std::string)> func);

	private:
		std::string name;
		std::string description;
		_owner owner;

		std::function<std::string(std::string)> on_command_run;
		std::function<std::string(std::string)> answer_interpretation;
};

#endif
