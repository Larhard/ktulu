#include "some_useful_sets.hpp"

std::vector<std::string> get_ktulu_error_codes() {
	std::vector<std::string> result;

	result.push_back("Unknown error.\n");
	result.push_back("Wrong command.\n");
	result.push_back("Avaible only in game.\n");
	result.push_back("Avaible only in lobby.\n");
	result.push_back("Avaible only in game choice.\n");
	result.push_back("You cannot do that in this time of day.\n");
	result.push_back("You already have a name.\n");

	return result;
}
