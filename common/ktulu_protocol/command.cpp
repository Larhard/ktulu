#include "command.hpp"

command::command() { }
command::command(
	std::string name,
	std::string description,
	_owner owner,
	std::function<std::string(std::string)> on_command_run,
	std::function<std::string(std::string)> answer_interpretation
) {
	this->name = name;
	this->description = description;
	this->owner = owner;
	this->on_command_run = on_command_run;
	this->answer_interpretation = answer_interpretation;
}

std::string command::get_name() {
	return this->name;
}

std::string command::get_description() {
	return this->description;
}

std::string command::run_me(std::string s) {
	return this->on_command_run(s);
}

_owner command::get_owner() {
	return this->owner;
}

std::string command::interpret_my_answer(std::string s) {
	return this->answer_interpretation(s);
}

void command::set_on_command_run_function(std::function<std::string(std::string)> func) {
	this->on_command_run = func;
}

void command::set_answer_interpretation(std::function<std::string(std::string)> func) {
	this->answer_interpretation = func;
}
