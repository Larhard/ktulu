#ifndef SERVER_SEARCHER_H
#define SERVER_SEARCHER_H

#include <boost/asio.hpp>
#include <memory>
#include <vector>

namespace Ktulu {
  struct ServerEndpoint {
    boost::asio::ip::udp::endpoint endpoint;
    std::string hello_message;
  };

  class ServerSeracher {
    private:
      class Impl;
      std::unique_ptr<Impl> pimpl;
      void do_receive();

    public:
      ServerSeracher(short port, const std::string &hello_message, const std::string &regexp=".*");
      ~ServerSeracher();

      std::vector<ServerEndpoint> search(int timeout);
  };

  std::vector<ServerEndpoint> search_servers(short port, int timeout, const std::string &hello_message, const std::string &regexp=".*");

  std::ostream & operator<<(std::ostream &stream, const ServerEndpoint &server_endpoint);
}

#endif // SERVER_SEARCHER_H
