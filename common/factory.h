#ifndef FACTORY_H
#define FACTORY_H

#include <memory>

template<typename T, typename... Args>
  // deprecated
  class Factory {
    private:
    public:
      static std::shared_ptr<T> create(Args&... args)
      {
        std::shared_ptr<T> product(new T(args...));
        product->_self = product;
        return product;
      }
  };

template<typename T, typename... Args>
  std::shared_ptr<T> create(Args... args)
  {
    std::shared_ptr<T> product(new T(args...));
    product->_self = product;
    return product;
  }

template<typename T>
  class Producible {
    public:
      std::weak_ptr<T> _self;
      std::shared_ptr<T> self()
      {
        return _self.lock();
      }
  };

#endif // FACTORY_H
