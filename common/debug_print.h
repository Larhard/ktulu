#ifndef DEBUG_PRINT_H
#define DEBUG_PRINT_H

#ifdef NDEBUG
#ifndef VERBOSE
#define NVERBOSE
#endif // VERBOSE
#endif // NDEBUG

#include <iostream>

namespace Debug {

    template<typename T>
        void write(std::ostream &stream, T value) {
            stream << value;
        }

    void printto(std::ostream &stream);

    template<typename T, typename... Rest>
        void printto(std::ostream &stream, const T& value, const Rest&... rest) {
            write(stream, value);
            if (sizeof...(Rest) > 0) {
                stream << " ";
            }
            printto(stream, rest...);
        }

    template<typename... T>
        void print(const T&... values) {
            printto(std::cout, values...);
        }

    template<typename... T>
        void perr(const T&... values) {
            printto(std::cerr, values...);
        }

    template<typename T>
        void printallto(std::ostream &stream, const T begin, const T end) {
            for (auto k = begin; k != end; ++k) {
                if (k != begin) {
                    stream << " ";
                }
                write(stream, *k);
            }
            stream << std::endl;
        }

    template<typename T>
        void printall(const T begin, const T end) {
            printallto(std::cout, begin, end);
        }

    template<typename T>
        void perrall(const T begin, const T end) {
            printallto(std::cerr, begin, end);
        }

#ifdef NVERBOSE

    template<typename... T>
        void dbg_print(const T&... values) {}

    template<typename... T>
        void dbg_perr(const T&... values) {}

    template<typename T>
        void dbg_printall(const T begin, const T end) {}

    template<typename T>
        void dbg_perrall(const T begin, const T end) {}

#else // VERBOSE

    template<typename... T>
        void dbg_print(const T&... values) {
            print(values...);
        }

    template<typename... T>
        void dbg_perr(const T&... values) {
            perr(values...);
        }

    template<typename T>
        void dbg_printall(const T begin, const T end) {
            printall(begin, end);
        }

    template<typename T>
        void dbg_perrall(const T begin, const T end) {
            perrall(begin, end);
        }

#endif // VERBOSE

}

#endif // DEBUG_PRINT_H
